# Team-Web

## Installation

Before installing, [download and install Node.js](https://nodejs.org/en/download/).
Node.js 9.6.1 or higher is required.

Installation is done using the
[`npm install` command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```bash
npm install
```

## Configuration

In a dev environment you can change the configuration by changing the .env file in the root of the server folder.

In a prod environment you shall set the config variables as environment variables.

## Quick start

Start the server:

```bash
npm start
```

## Database

To initialize and seed the database for first run, change the {force: false} to {force: true} in db.sequelize.sync.

Then start the app with:

```bash
npm start
```

And then seed the database with (Sequelize CLI need to be install):

```bash
sequelize db:seed:all
```

Remember to switch back force: true to false again after first run.
