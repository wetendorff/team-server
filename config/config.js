const env = require('./env');

module.exports = {
  development: {
    username: env.DATABASE_USERNAME,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME,
    host: env.DATABASE_HOST,
    dialect: env.DATABASE_DIALECT,
  },
  test: {
    username: '',
    password: null,
    database: '',
    host: '',
    dialect: '',
  },
  production: {
    username: '',
    password: null,
    database: '',
    host: '',
    dialect: '',
  },
};
