require('dotenv').config();

module.exports = {
  HTTP_PORT: process.env.HTTP_PORT || 3000,
  DATABASE_PORT: process.env.DATABASE_PORT || 3306,
  DATABASE_DIALECT: process.env.DATABASE_DIALECT || 'mysql',
  DATABASE_HOST: process.env.DATABASE_HOST || 'localhost',
  DATABASE_NAME: process.env.DATABASE_NAME || 'team-web',
  DATABASE_USERNAME: process.env.DATABASE_USERNAME || 'root',
  DATABASE_PASSWORD: process.env.DATABASE_PASSWORD || 'lwn2209',
  APP: process.env.APP || 'dev',
  JWT_EXPIRATION: process.env.JWT_EXPIRATION || '10000',
  JWT_ENCRYPTION: process.env.JWT_ENCRYPTION || 'SuperSecurePassword',
};
