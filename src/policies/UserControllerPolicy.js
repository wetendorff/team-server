const Joi = require('joi');
const responseUtil = require('../utils/responseUtil');

module.exports = {
  create(req, res, next) {
    const schema = {
      fullName: Joi.string().min(1).max(64).required(),
      email: Joi.string().max(128).email().required(),
      phone: Joi.string().min(7).max(20).required(),
      password: Joi.string().min(8).max(64).regex(new RegExp('^[a-zA-Z0-9]*$')),
    };

    const { error, value } = Joi.validate(req.body, schema);

    if (error) {
      responseUtil.ReturnError(res, error.message, 422);
    } else {
      next();
    }
  },
};