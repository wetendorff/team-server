const { Strategy } = require('passport-jwt');
const { User } = require('../models');
const env = require('../../config/env');
const promiseUtil = require('../utils/promiseUtil');


module.exports = (passport) => {
  const opts = {
    jwtFromRequest: (req) => {
      let token = null;
      if (req && req.cookies) {
        token = req.cookies.jwt;
      }
      return token;
    },
    secretOrKey: env.JWT_ENCRYPTION,
  };

  passport.use(new Strategy(opts, async (jwtPayload, done) => {
    const [err, user] = await promiseUtil.to(User.findById(jwtPayload.userId));

    if (err) return done(err, false);

    if (user) {
      return done(null, user);
    }

    return done(null, false);
  }));
};
