const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');

const { Club } = require('../models');

const create = async (clubInfo) => {
  const [err, club] = await promiseUtil.to(Club.create(clubInfo));
  if (err) {
    responseUtil.ThrowError('Cannot create club');
  }
  return club;
};
module.exports.create = create;
