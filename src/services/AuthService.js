const validator = require('validator');

const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');
const { User } = require('../models');


const getUniqueKeyFromBody = (body) => {
  let { uniqueKey } = body;

  if (typeof uniqueKey === 'undefined') {
    if (typeof body.email !== 'undefined') {
      uniqueKey = body.email;
    } else if (typeof body.phone !== 'undefined') {
      uniqueKey = body.phone;
    } else {
      uniqueKey = null;
    }
  }
  return uniqueKey;
};
module.exports.getUniqueKeyFromBody = getUniqueKeyFromBody;


const createUser = async (userInfo) => {
  let err;
  let user;

  const authInfo = {
    status: 'create',
  };

  const uniqueKey = getUniqueKeyFromBody(userInfo);
  if (!uniqueKey) responseUtil.ThrowError('An email or phone number was not entered.');

  if (validator.isEmail(uniqueKey)) {
    authInfo.method = 'email';
    userInfo.email = uniqueKey;

    [err, user] = await promiseUtil.to(User.create(userInfo));
    if (err) {
      responseUtil.ThrowError('User already exists with that email');
    }

    return user;
  } else if (validator.isMobilePhone(uniqueKey, 'any')) {
    authInfo.method = 'phone';
    userInfo.phone = uniqueKey;

    [err, user] = await promiseUtil.to(User.create(userInfo));
    if (err) responseUtil.ThrowError('User already exists with that phone number');

    return user;
  }
  responseUtil.ThrowError('A valid email or phone number was not entered.');
  return null;
};
module.exports.createUser = createUser;


const authUser = async (userInfo) => {
  const uniqueKey = getUniqueKeyFromBody(userInfo);
  const authInfo = {
    status: 'login',
  };
  let user;
  let err;

  if (!uniqueKey) {
    responseUtil.ThrowError('Please enter an email or phone number to login');
  }

  if (!userInfo.password) {
    responseUtil.ThrowError('Please enter a password to login');
  }

  if (validator.isEmail(uniqueKey)) {
    authInfo.method = 'email';

    [err, user] = await promiseUtil.to(User.findOne({
      where: {
        email: uniqueKey,
      },
    }));

    if (err) {
      responseUtil.ThrowError(err.message);
    }
  } else if (validator.isMobilePhone(uniqueKey, 'any')) {
    authInfo.method = 'phone';

    [err, user] = await promiseUtil.to(User.findOne({
      where: {
        phone: uniqueKey,
      },
    }));

    if (err) {
      responseUtil.ThrowError(err.message);
    }
  } else {
    responseUtil.ThrowError('A valid email or phone number was not entered');
  }

  if (!user) {
    responseUtil.ThrowError('Not registered');
  }

  [err, user] = await promiseUtil.to(user.comparePassword(userInfo.password));
  if (err) {
    responseUtil.ThrowError(err.message);
  }

  return user;
};
module.exports.authUser = authUser;
