const { Club } = require('../models');
const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');
const clubService = require('../services/ClubService');

const HttpStatus = require('http-status-codes');


module.exports = {
  async get(req, res) {
    const { id } = req.params;

    const [err, club] = await promiseUtil.to(Club.find({
      where: {
        id,
      },
    }));

    if (err) {
      return responseUtil.ReturnError(res, err.message);
    }

    if (!club) {
      return responseUtil.ReturnError(res, 'Not found', HttpStatus.NOT_FOUND);
    }

    return responseUtil.ReturnSuccess(res, club);
  },

  async getAll(req, res) {
    const [err, clubs] = await promiseUtil.to(Club.findAll());

    if (err) {
      return responseUtil.ReturnError(res, err.message);
    }

    if (!clubs) {
      return responseUtil.ReturnSuccess(res, [], HttpStatus.OK);
    }

    return responseUtil.ReturnSuccess(res, clubs);
  },

  async create(req, res) {
    const { body } = req;

    const [err, club] = await promiseUtil.to(clubService.create(body));
    if (err) {
      return responseUtil.ReturnError(res, err, 422);
    }

    return responseUtil.ReturnSuccess(res, {
      message: 'Successfully created new club.',
      club: club.toJSON(),
    }, 201);
  },
};
