const { User } = require('../models');
const authService = require('../services/AuthService');
const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');

const HttpStatus = require('http-status-codes');

module.exports = {
  async create(req, res) {
    const { body } = req;

    const [err, user] = await promiseUtil.to(authService.createUser(body));

    if (err) {
      return responseUtil.ReturnError(res, err, 422);
    }

    res.cookie('jwt', user.getJWT(), { secure: true, httpOnly: true });

    return responseUtil.ReturnSuccess(res, {
      message: 'Successfully created new user.',
      user: user.toJSON(),
    }, 201);
  },

  async login(req, res) {
    const { body } = req;

    const [err, user] = await promiseUtil.to(authService.authUser(body));
    if (err) return responseUtil.ReturnError(res, err, 422);

    res.cookie('jwt', user.getJWT(), { secure: true, httpOnly: true });

    return responseUtil.ReturnSuccess(res, {
      message: 'Successfully authenticated user',
      user: user.toJSON(),
    });
  },

  async get(req, res) {
    const { id } = req.params;

    let { user } = req;
    let err;

    if (user.role === 'admin') {
      ([err, user] = await promiseUtil.to(User.find({
        where: {
          id,
        },
      })));

      if (err) {
        return responseUtil.ReturnError(res, err.message);
      }
      if (!user) {
        return responseUtil.ReturnSuccess(res, 'No Content', HttpStatus.NO_CONTENT);
      }
    }

    return responseUtil.ReturnSuccess(res, { user: user.toJSON() });
  },

  async getAll(req, res) {
    const { user } = req;
    let err;
    let users;

    if (user.role === 'admin') {
      ([err, users] = await promiseUtil.to(User.findAll()));
    } else {
      ([err, users] = await promiseUtil.to(User.find({
        where: { id: user.id },
      })));
    }

    if (err) {
      return responseUtil.ReturnError(res, err.message);
    }

    if (!users) {
      return responseUtil.ReturnSuccess(res, []);
    }

    return responseUtil.ReturnSuccess(res, users);
  },

  async update(req, res) {
    const { user: _user, body } = req;
    let user = _user;
    let err;

    user.set(body);

    ([err, user] = await promiseUtil.to(user.save()));

    if (err) {
      if (err.message === 'Validation error') {
        err = 'The email address or phone number is already in use';
      }
      return responseUtil.ReturnError(res, err);
    }

    return responseUtil.ReturnSuccess(res, {
      message: `Updated User: ${user.email}`,
    });
  },

  async remove(req, res) {
    const { user: _user } = req;
    let user = _user;

    const [err, _newUser] = await promiseUtil.to(user.destroy());
    user = _newUser;

    if (err) {
      return responseUtil.ReturnError(res, 'error occurred trying to delete user');
    }

    return responseUtil.ReturnSuccess(res, {
      message: 'Deleted User',
    }, 204);
  },

};
