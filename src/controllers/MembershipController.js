const { Membership } = require('../models');
const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');

const HttpStatus = require('http-status-codes');


module.exports = {
  async getAll(req, res) {
    const [err, memberships] = await promiseUtil
      .to(Membership.findAll({ include: [{ all: true, attributes: { exclude: ['password'] } }] }));

    if (err) {
      return responseUtil.ReturnError(res, err.message);
    }

    if (!memberships) {
      return responseUtil.ReturnSuccess(res, [], HttpStatus.OK);
    }

    return responseUtil.ReturnSuccess(res, memberships);
  },
};
