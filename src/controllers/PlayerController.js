const { Player } = require('../models');
const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');

const HttpStatus = require('http-status-codes');


module.exports = {
  async getPlayersByUserId(req, res) {
    const { id } = req.params;

    const [err, players] = await promiseUtil.to(Player.findAll({
      where: {
        UserId: id,
      },
    }));

    if (err) {
      return responseUtil.ReturnError(res, err.message);
    }

    if (!players) {
      return responseUtil.ReturnError(res, 'Not found', HttpStatus.NOT_FOUND);
    }

    return responseUtil.ReturnSuccess(res, players);
  },
};
