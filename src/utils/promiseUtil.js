const parseError = require('parse-error');

module.exports = {
  to(promise) {
    return promise
      .then(data => [null, data]).catch(err =>
        [parseError(err)]);
  },
};
