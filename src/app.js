const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const env = require('../config/env');
const parseError = require('parse-error');
const { sequelize } = require('./models');

// Set up express
const app = express();
app.use(cookieParser());
app.use(morgan('combined'));
app.use(express.json());
app.use(cors());

// Routes
require('./routes')(app);

// Handle all uncaught promise rejections
process.on('unhandledRejection', (error) => {
  console.error('Uncaught Error', parseError(error));
});

// Database
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');

    // Sync database if in dev environment
    if (env.APP === 'dev') {
      sequelize.sync({ force: false }).then(); // create table if they not already exist
    }

    // Start server
    app.listen(env.HTTP_PORT, () => console.log(`Example app listening on port ${env.HTTP_PORT}`));
  })
  .catch((err) => {
    console.error('Unable to connect to database: ', err);
  });

