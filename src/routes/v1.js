const express = require('express');
const passport = require('passport');

const UserController = require('../controllers/UserController');
const ClubController = require('../controllers/ClubController');
const PlayerController = require('../controllers/PlayerController');
const MembershipController = require('../controllers/MembershipController');

const UserControllerPolicy = require('../policies/UserControllerPolicy');

const router = express.Router();

// Initialize passport middleware
require('../middleware/passport')(passport);

// Routes
router.post('/users/login', UserController.login);
router.post('/users', UserControllerPolicy.create, UserController.create);

router.get(
  '/users',
  passport.authenticate('jwt', { session: false }),
  UserController.getAll,
);

router.put(
  '/users',
  passport.authenticate('jwt', { session: false }),
  UserController.update,
);

router.delete(
  '/users',
  passport.authenticate('jwt', { session: false }),
  UserController.remove,
);

router.get(
  '/users/:id',
  passport.authenticate('jwt', { session: false }),
  UserController.get,
);

router.get(
  '/clubs',
  passport.authenticate('jwt', { session: false }),
  ClubController.getAll,
);

router.get(
  '/clubs/:id',
  passport.authenticate('jwt', { session: false }),
  ClubController.get,
);

router.post(
  '/clubs',
  ClubController.create,
);

router.get(
  '/users/:id/players',
  passport.authenticate('jwt', { session: false }),
  PlayerController.getPlayersByUserId,
);

router.get(
  '/memberships',
  passport.authenticate('jwt', { session: false }),
  MembershipController.getAll,
);

module.exports = router;
