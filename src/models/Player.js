module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('Player', {
    jerseyNo: dataTypes.INTEGER,
    jerseyName: dataTypes.STRING(10),
  }, {
    timestamps: true,
    paranoid: true,
  });

  return Model;
};
