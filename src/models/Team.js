module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('Team', {
    name: dataTypes.STRING(64),
  }, {
    timestamps: true,
    paranoid: true,
  });

  return Model;
};
