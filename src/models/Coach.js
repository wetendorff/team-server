module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('Coach', {
    title: dataTypes.STRING,
  }, {
    timestamps: true,
    paranoid: true,
  });

  return Model;
};
