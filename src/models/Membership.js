module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('Membership', {
    fee: dataTypes.DECIMAL(10, 2),
  }, {
    timestamps: true,
    paranoid: true,
  });

  Model.associate = (models) => {
    Model.belongsTo(models.User);
    Model.belongsTo(models.Team);
  };

  return Model;
};
