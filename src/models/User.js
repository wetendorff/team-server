const bcrypt = require('bcrypt-promise');
const jwt = require('jsonwebtoken');
const env = require('../../config/env');
const responseUtil = require('../utils/responseUtil');
const promiseUtil = require('../utils/promiseUtil');

module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('User', {
    fullName: dataTypes.STRING(64),
    email: {
      type: dataTypes.STRING(128),
      allowNull: true,
      unique: true,
      validate: { isEmail: { msg: 'Email is invalid.' } },
    },
    phone: {
      type: dataTypes.STRING(20),
      allowNull: true,
      unique: true,
      validate: {
        len: { args: [7, 20], msg: 'Phone number is invalid' },
        isNumeric: { msg: 'Not a valid phone number.' },
      },
    },
    password: dataTypes.STRING(64),
    profile: {
      type: dataTypes.STRING(10),
      allowNull: false,
      defaultValue: 'main',
      validate: {
        isIn: [['parent', 'main']],
      },
    },
  }, {
    timestamps: true,
    paranoid: true,
  });

  Model.beforeSave(async (user) => {
    if (user.changed('password')) {
      const [_err, salt] = await promiseUtil.to(bcrypt.genSalt(10));
      let err = _err;

      if (err) responseUtil.ThrowError(err.message, true);

      const [__err, hash] = await promiseUtil.to(bcrypt.hash(user.password, salt));
      err = __err;

      if (err) responseUtil.ThrowError(err.message, true);

      user.password = hash;
    }
  });

  Model.prototype.comparePassword = async function comparePassword(password) {
    if (!this.password) responseUtil.ThrowError('Password not set');

    const [err, pass] = await promiseUtil.to(bcrypt.compare(password, this.password));

    if (err) responseUtil.ThrowError(err);
    if (!pass) responseUtil.ThrowError('Invalid password');

    return this;
  };

  Model.prototype.getJWT = function getJWT() {
    const expirationTime = parseInt(env.JWT_EXPIRATION, 10);
    const token = jwt.sign(
      { user: this.toJSON() },
      env.JWT_ENCRYPTION,
      {
        expiresIn: expirationTime,
      },
    );

    return token;
  };

  Model.prototype.toJSON = function toJSON() {
    const json = Object.assign({}, this.get());

    // remove password and deleted at field from output
    delete json.password;

    return json;
  };

  return Model;
};
