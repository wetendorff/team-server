module.exports = (sequelize, dataTypes) => {
  const Model = sequelize.define('Club', {
    name: {
      type: dataTypes.STRING(32),
      allowNull: false,
      unique: true,
    },
  }, {
    timestamps: true,
    paranoid: true,
  });

  return Model;
};
