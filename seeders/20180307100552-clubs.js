

module.exports = {
  up: queryInterface => queryInterface.bulkInsert('clubs', [{
    name: 'Amager SK',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    name: 'FCK',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    name: 'SIF',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('clubs', null, {}),
};
