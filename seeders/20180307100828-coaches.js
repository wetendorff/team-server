

module.exports = {
  up: queryInterface => queryInterface.bulkInsert('coaches', [{
    title: 'Træner',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('coaches', null, {}),
};
