

module.exports = {
  up: queryInterface => queryInterface.bulkInsert('users', [{
    fullName: 'Lars Wetendorff Nielsen',
    email: 'larswn@me.com',
    phone: '42572209',
    password: '$2a$10$oTrYZKqiDCPxjvPko1eEROx0aubvOnIQVh5hnkkjH/s86S4cspVvm',
    profile: 'main',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('user', null, {}),
};
