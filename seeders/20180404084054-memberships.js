module.exports = {
  up: queryInterface => queryInterface.bulkInsert('Memberships', [{
    fee: 100.00,
    UserId: 1,
    TeamId: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('Memberships', null, {}),
};
