

module.exports = {
  up: queryInterface => queryInterface.bulkInsert('players', [{
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('players', null, {}),
};
