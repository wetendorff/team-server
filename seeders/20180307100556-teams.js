

module.exports = {
  up: queryInterface => queryInterface.bulkInsert('teams', [{
    name: 'U8',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    name: 'U10 drenge',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    name: 'U10 piger',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    name: 'U8',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: queryInterface => queryInterface.bulkDelete('teams', null, {}),
};
